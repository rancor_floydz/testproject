
$(document).ready(function() {
	
	$('#msg_edit').hide();

	$('#addnewmsg').on('click',function() {
		$('#msg_form input').val('');
		$('#msg_form textarea').val('');
		$("#imd_save_img").empty();
	});

	//----action if one of the message is click to delete
	$('#msg_list').on('click', '.msg_delete', function() {
		$('#deleteModal').modal('show');
		$("#msg_delete").prop('value', this.value);
		$("#imd_delete_img").empty();
	});
	//----action after YES or NO of modal for delete
	$('#msg_delete').on('click',function() {
			$.ajax({
					url: 'message.php',
					type: 'POST',
					data: { id:this.value,flag:'msg_delete'},
					dataType:'json',
					beforeSend:function(){

			           $("#imd_delete_img").append('<img alt="" src="images/loading.gif">');
			        },
					success:function(response){
						// console.log(response)
						// $('.error').removeClass('error');
						// $('#msg_form input').val('');
						// $('#msg_form textarea').val('');
						$("#imd_delete_img").empty();
						$("#imd_delete_img").append('<img alt="" src="images/success.gif">');
						search(null);					
					}
			});
	});

	$('#msg_list').on('click', '.msg_edit', function() {
		$('#myModal').modal('show');
		$("#msg_edit").prop('value', this.value);
		$("#imd_edit_img").empty();
			$.ajax({
					url: 'message.php',
					type: 'POST',
					data: { id:this.value,flag:'msg_searchbyID'},
					dataType:'json',
					 beforeSend:function(){
					 	$('#msg_form input').val('');
						$('#msg_form textarea').val('');
						$('#msg_edit').show();
						$('#msg_save').hide();						
			        },
					success:function(response){
						$('#fname').val(response.fname);
						$('#mname').val(response.mname);
						$('#lname').val(response.lname);
						$('#msg').val(response.message);
					}
			});
	});

	
	$('#msg_save').on('click',function(){		
		var fname = $('#fname').val();
		var mname = $('#mname').val();
		var lname = $('#lname').val();
		var msg = $('#msg').val();
		var flag = this.id;
		var date = '';

		if(validateInput(fname,mname,lname,msg)){
			$.ajax({
					url: 'message.php',
					type: 'POST',
					data: { fname:fname,mname:mname,lname:lname,msg:msg,date:date,flag:flag},
					dataType:'json',
					beforeSend:function(){
			           $("#imd_save_img").append('<img alt="" src="images/loading.gif">');
			        },
					success:function(response){
						$('.error').removeClass('error');
						$('#msg_form input').val('');
						$('#msg_form textarea').val('');
						$("#imd_save_img").empty();
						$("#imd_save_img").append('<img alt="" src="images/success.gif">');
						search(null);
					}
			});
		}
	});

	$('#msg_edit').on('click',function(){		
		var fname = $('#fname').val();
		var mname = $('#mname').val();
		var lname = $('#lname').val();
		var msg = $('#msg').val();
		var flag = this.id;
		var date = '';
		
		if(validateInput(fname,mname,lname,msg)){
			$.ajax({
					url: 'message.php',
					type: 'POST',
					data: { fname:fname,mname:mname,lname:lname,msg:msg,date:date,flag:flag,msg_id:this.value},
					dataType:'json',
					 beforeSend:function(){
			           $("#imd_edit_img").append('<img alt="" src="images/loading.gif">');
			        },
					success:function(response){
						$('.error').removeClass('error');
						$('#msg_form input').val('');
						$('#msg_form textarea').val('');
						$("#imd_edit_img").empty();
						$("#imd_edit_img").append('<img alt="" src="images/success.gif">');
						search(null);
					}
			});
		}
	});

	search(null);
	
	

});

function search(input){
	$.ajax({
		url: 'message.php',
		type: 'POST',
		data: { flag:'msg_search',input: input},
		dataType:'json',
		success:function(response){
			$("#msg_list").empty();
			$("#msg_list").append(response);
			// $("#msg_list").append('<tr>aw</tr>');
			// console.log(response);
		}
	});
}

function validateInput(fname,mname,lname,msg){
	if(fname == '' ||mname == '' ||lname == ''||msg == ''){
			if(fname == ''){
				$('#fname').addClass('error');
			}
			if(mname == ''){
				$('#mname').addClass('error');
			}
			if(lname == ''){
				$('#lname').addClass('error');
			}
			if(msg == ''){
				$('#msg').addClass('error');
			}
		return false;
	}else{
		return true;
	}
}

