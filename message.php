<?php
include('model/dbmessage.php');
$dbmessage = new DBMessage();

if($_POST['flag'] == 'msg_save'){
	$return = $dbmessage->save($_POST);
	echo json_encode($return);
}

if($_POST['flag'] == 'msg_edit'){
	$return = $dbmessage->edit($_POST);
	echo json_encode($return);
}
if($_POST['flag'] == 'msg_delete'){
	$return = $dbmessage->delete($_POST);
	echo json_encode($return);
}

if($_POST['flag'] == 'msg_search'){
	$return = $dbmessage->search($_POST);
	$tr = '';
	if($return == false){
		$tr .= '<tr class="danger"><td colspan="4" style="text-align:center;">No data found</td></tr>';
	}else{
		// creating the EDIT and DELETE Button in every DATA_ROW and also viewing each data in a row
			while ($var = mysql_fetch_array($return)) {
		            $tr .= "<tr  class='success'>";
		            $tr .= "<td><button value=".$var['msg_id']." class='btn btn-info btn-xs msg_edit'>
		            			<span class='glyphicon glyphicon-edit' aria-hidden='true'></span></button>";
		            $tr .= "<span aria-hidden='true'>&nbsp;&nbsp;</span><button value=".$var['msg_id']." class='btn btn-danger btn-xs msg_delete '><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button></td>";
		            $tr .= "<td>".$var['date_created']."</td>";
		            $tr .= "<td>".ucfirst($var['message'])."</td>";
		            $tr .= "<td>". ucfirst($var['lname']).', '. ucfirst($var['fname']) .' '. ucfirst($var['mname'][0]) . ".</td>";
		            $tr .= "</tr>";
		    }
	}
	echo json_encode($tr);
}

if($_POST['flag'] == 'msg_searchbyID'){
	$return = $dbmessage->searchbyID($_POST['id']);
	echo json_encode(mysql_fetch_object($return));
}

?>