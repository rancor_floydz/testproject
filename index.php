<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
    
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Test Project</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">Message List</a>
                    </li>
                   <!--  <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row-fluid">          
                <div class="row">
                    <button id="addnewmsg" type="button" class="pull-left btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                      Add Messages <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </div>
                <br>
                <div class="row">
                    <div class="table-responsives" id="table_msg">
                        <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Action</th> 
                                <th>DATE</th> 
                                <th>Message</th>
                                <th>Name</th>
                            </tr>
                        </thead> 
                        <tbody id="msg_list">
                            <!-- date here from js -->
                        </tbody>
                        </table>
                    </div>
                </div>
                
                <!-- <nav>
                  <ul class="pagination">
                    <li>
                      <a href="#" aria-label="First">
                        <span aria-hidden="true">&lsaquo;</span>
                      </a>
                    </li>
                     <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="2">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                     <li>
                      <a href="#" aria-label="Last">
                        <span aria-hidden="true">&rsaquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav> -->
            
        </div>
        <!-- /.row -->
                

    </div><!-- /.container -->

    <!-- MODAL FOR ADD  -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Message</h4>
                      </div>
                    
                      <div  id="msg_form" class="modal-body">
                      
                       <!-- FOR ADD -->
                         <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon">Full Name</div>                              
                              <input type="text" class="form-control" id="fname" placeholder="First Name" maxlength="25" minlength="2" required>
                              <input type="text" class="form-control" id="mname" placeholder="Middle Name" maxlength="25" minlength="2" required>
                              <input type="text" class="form-control" id="lname" placeholder="Last Name" maxlength="25" minlength="2" required>
                            </div>
                          </div>
                        <textarea id="msg" class="form-control" rows="3" placeholder="Input your message here.." required></textarea>
                        
                        <!--  -->
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="msg_save" type="submit" class="btn btn-primary">Save Message <div id="imd_save_img"></div></button>
                        <button id="msg_edit" type="submit" class="btn btn-primary" value="">Save Message <div id="imd_edit_img"></div></button>
                      </div>
                     
                    </div>
                  </div>
                </div>
    <!-- END MODAL FOR ADD  -->
     <!-- MODAL FOR ADD  -->
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete?</h4>
                      </div>
                    
                      <!-- <div  id="msg_form" class="modal-body">
                      
                         </div> -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button id="msg_delete" type="submit" class="btn btn-success" value="">Yes<div id="imd_delete_img"></div></button>
                      </div>
                     
                    </div>
                  </div>
                </div>
    <!-- END MODAL FOR ADD  -->
    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
     <script src="js/main.js"></script>

</body>

</html>
